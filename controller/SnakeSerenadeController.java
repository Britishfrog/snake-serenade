package snakeserenade.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import snakeserenade.model.DataSnake;
import snakeserenade.model.SerenadeResult;
import snakeserenade.model.SingerSnake;
import snakeserenade.model.SingerSnakeEnsemble;
import snakeserenade.parser.DataParser;
import snakeserenade.parser.impl.CsvDataParser;
import snakeserenade.utils.ConversionUtils;

/**
 * 
 * The SnakeSerenadeController class allows the parsing, the linking and the composing of the snake serenade
 *
 */

public class SnakeSerenadeController {

    private DataParser dataParser;
    private int maximumRepeats;

    public SnakeSerenadeController(String csvFile, String maximumRepeats) {
        this.dataParser = new CsvDataParser(csvFile);
        this.maximumRepeats = ConversionUtils.convertMaximumRepeats(maximumRepeats);
    }//SnakeSerenadeController

    public void run() {
        
        SingerSnakeEnsemble ensemble = dataParser.parse();
        
        // We have in the Ensemble a Map with the DataSnakes and a map with the SingerSnakes so let's link them together.
	Map<String, SingerSnake> singerSnakesMap = ensemble.getSingerSnakesMap();
	Map<String, DataSnake> dataSnakesMap = ensemble.getDataSnakesMap();
                
	for (int i = 0; i < singerSnakesMap.size(); i++) {
                    
            SingerSnake singerSnake = ensemble.getSingerSnakeAtPos(i);
            DataSnake dataSnake = dataSnakesMap.get(singerSnake.getName());
        	
            singerSnake.setHeadEndSnake(ensemble.getSingerSnakeAtPos(dataSnake.getHead()));
            singerSnake.setTailEndSnake(ensemble.getSingerSnakeAtPos(dataSnake.getTail()));
                    
        }
		
	// Run the snaky serenade for the given snake ensemble and the maximum repeats per snake.
	SnakeSerenadeComposer composer = new SnakeSerenadeComposer(ensemble, maximumRepeats);
	List<SerenadeResult> serenadeResultList = composer.run();
	Collections.sort(serenadeResultList, new SortSerenadeResultArrayList());
		
	int numberOfSings = serenadeResultList.get(0).getSingCount();
	String result = serenadeResultList.get(0).getSerenadeResult();
		
	// Print the result
	System.out.println("A maximal length of " + maximumRepeats + "-MRPS song for this " 
			   + singerSnakesMap.size() + "-snake ensemble has " + numberOfSings + " sings:");
	System.out.println(result);
        
    }//run
    
    
    //Class implements interface in order to compare the results.
    public class SortSerenadeResultArrayList implements Comparator<SerenadeResult> {

		@Override
		public int compare(SerenadeResult sr1, SerenadeResult sr2) {
			return sr2.getSingCount() - sr1.getSingCount();
		}//compare
    	
    }//SortSerenadeResultsArrayList
    
}//SnakeSerenadeController