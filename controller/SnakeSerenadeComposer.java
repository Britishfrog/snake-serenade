package snakeserenade.controller;

import java.util.List;
import java.util.Map;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import snakeserenade.model.SerenadeResult;
import snakeserenade.model.SingerSnake;
import snakeserenade.model.SingerSnakeEnsemble;

/**
 * The composer takes the input it receives, namely:
 *  -the map
 *  -the maximum repeats
 * and composes the snake serenade from this data.
 *
 */
public class SnakeSerenadeComposer {
	
	private Map<String, Integer> visitsMap = Maps.newHashMap(); 
	private int singCount;

	private SingerSnakeEnsemble singerSnakeEnsemble;
	
	private int maximumRepeats;
	
	public SnakeSerenadeComposer(SingerSnakeEnsemble singerSnakeEnsemble, int maximumRepeats) {
		this.singerSnakeEnsemble = singerSnakeEnsemble;
		this.maximumRepeats = maximumRepeats;
	}

	public List<SerenadeResult> run() {
		List<SerenadeResult> resultList = Lists.newArrayList();
		Map<String, SingerSnake> singerSnakesMap = singerSnakeEnsemble.getSingerSnakesMap();
		for (int i = 0; i < singerSnakesMap.size(); i++) {
			// Pick up every Snake in the ensemble as parent.
			SingerSnake singerSnake = singerSnakeEnsemble.getSingerSnakeAtPos(i);
			// Initialize the visits from this node
			for (int j = 0; j < singerSnakesMap.size(); j++) {
				visitsMap.put(singerSnakeEnsemble.getSingerSnakeAtPos(j).getName(), 0);
			}
			
			// Initialize the song count and increment it
			singCount = 0;
			singCount++;
			
			// Update the visits number
			visitsMap.put(singerSnake.getName(), 1);
			
			String result = searchUsingRoot(singerSnake);
			
			SerenadeResult serenadeResult = new SerenadeResult();
			serenadeResult.setSingCount(singCount);
			serenadeResult.setSerenadeResult(result);
			resultList.add(serenadeResult);
		}
		
		return resultList;
	}

	private String searchUsingRoot(SingerSnake singerSnake) {
		String heads = "", tails = "";
		int visits;
		// Search for the head and tail links, if they are available, search again using them as parent.
		if (singerSnake.getHeadEndSnake() != null) {
			visits = visitsMap.get(singerSnake.getHeadEndSnake().getName());
			if (visits < maximumRepeats + 1) {
				visitsMap.put(singerSnake.getHeadEndSnake().getName(), visits + 1);
				singCount++;
				heads = ", " + searchUsingRoot((SingerSnake) singerSnake.getHeadEndSnake());
			}
		}
		
		if (singerSnake.getTailEndSnake() != null) {
			visits = visitsMap.get(singerSnake.getTailEndSnake().getName());
			if (visits < maximumRepeats + 1) {
				visitsMap.put(singerSnake.getTailEndSnake().getName(), visits + 1);
				singCount++;
				heads = ", " + searchUsingRoot((SingerSnake) singerSnake.getTailEndSnake());
			}
		}
		
		return singerSnake.getName() + heads + tails;
	}

}