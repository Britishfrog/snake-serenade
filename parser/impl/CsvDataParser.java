package snakeserenade.parser.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import snakeserenade.model.DataSnake;
import snakeserenade.model.SingerSnake;
import snakeserenade.model.SingerSnakeEnsemble;
import snakeserenade.model.Snake;
import snakeserenade.parser.DataParser;
import snakeserenade.utils.ConversionUtils;

/**
 * The CsvDataParser class allows the CSV input file to be read and parsed into a map
 */
public class CsvDataParser implements DataParser {

    private static final Logger logger = Logger.getLogger(CsvDataParser.class.getName());

    private String csvFileName;

    public CsvDataParser(String csvFileName) {
        this.csvFileName = csvFileName;
    }

    @Override
    public SingerSnakeEnsemble parse() {
    	SingerSnakeEnsemble singerSnakeEnsemble = new SingerSnakeEnsemble();
        BufferedReader reader = null;
        try {
        	reader = new BufferedReader(new FileReader(csvFileName));
            String line;
            while((line = reader.readLine()) != null) {
                // Convert the CSV line to a DataSnake object and add it to the SingerSnakeEnsemble
            	DataSnake dataSnake = ConversionUtils.convertCsvLineToSnake(line);
                singerSnakeEnsemble.addSnake(dataSnake);

                // Create a SingerSnake and add it to the SingerSnakeEnsemble
                SingerSnake singerSnake = new SingerSnake();
                singerSnake.setName(dataSnake.getName());
                singerSnakeEnsemble.addSnake(singerSnake);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error when reading from file");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.log(Level.SEVERE, "Error when closing file");
                }
            }
        }
        
        return singerSnakeEnsemble;
    }
    
}