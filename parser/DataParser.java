package snakeserenade.parser;

import snakeserenade.model.DataSnake;
import snakeserenade.model.SingerSnakeEnsemble;

/**
 * 
 * The DataParser class acts as an interface for parsing the snakes into a map
 * 
 */
public interface DataParser {

    SingerSnakeEnsemble parse();

}