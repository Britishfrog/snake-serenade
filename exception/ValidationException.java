package snakeserenade.exception;

/**
 * 
 * The ValidationException class is a thrown exception for validation errors
 *
 */
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 7053365356792123755L;

    public ValidationException() {
        super();
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable t) {
        super(message, t);
    }

}
