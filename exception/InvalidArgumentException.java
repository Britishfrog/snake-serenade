package snakeserenade.exception;

/**
 * 
 * The InvalidArgumentException class is a thrown exception for invalid arguments
 * 
 */

public class InvalidArgumentException extends Exception {

    private static final long serialVersionUID = 1555729783878142190L;

    public InvalidArgumentException() {
        super();
    }

    public InvalidArgumentException(String message) {
        super(message);
    }

    public InvalidArgumentException(String message, Throwable t) {
        super(message, t);
    }

}
