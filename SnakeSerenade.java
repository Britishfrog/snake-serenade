package snakeserenade;

import snakeserenade.controller.SnakeSerenadeController;
import snakeserenade.exception.InvalidArgumentException;

/**
 * This is the main class for the Snake Serenade program
 * It takes 2 arguments: 
 *      - the name of the CSV file containing the snake ensemble  ex: filepath/ensemble.csv
 *      - an integer representing the number of maximum repeats per snake (must be greater or equal to 0)
 *
 */

public class SnakeSerenade {

	public static void main(String[] args) throws InvalidArgumentException {
	    if (args.length != 2) {
	        throw new InvalidArgumentException("You must specify a [filename] and a [MRPS] as arguments.");
	    }

	    // Controller using the specified arguments
            System.out.println("\n[ Result using arguments : ]");
	    SnakeSerenadeController controller = new SnakeSerenadeController(args[0], args[1]);
	    controller.run();
            
            /*
            What follows is a predefined set of tests that pair 
            with the examples given in the Snaky_Serenades PDF file
            */
            
            //Test 1 using predefined arguments and snake model
            System.out.println("\n[ Test 1: ]");
            controller = new SnakeSerenadeController("test1.csv", "0");
            controller.run();
            controller = new SnakeSerenadeController("test1.csv", "1");
            controller.run();
            
            //Test 2 using predefined arguments and snake model
            System.out.println("\n[ Test 2: ]");
            controller = new SnakeSerenadeController("test2.csv", "0");
            controller.run();
            controller = new SnakeSerenadeController("test2.csv", "1");
            controller.run();
            
            //Test 3 using predefined arguments and snake model
            System.out.println("\n[ Test 3: ]");
            controller = new SnakeSerenadeController("test3.csv", "0");
            controller.run();
            controller = new SnakeSerenadeController("test3.csv", "1");
            controller.run();
            
            //Test 4 using predefined arguments and snake model
            System.out.println("\n[ Test 4 : ]");
            controller = new SnakeSerenadeController("test4.csv", "0");
            controller.run();
            controller = new SnakeSerenadeController("test4.csv", "1");
            controller.run();
            
            //Test 4 with alternate format using predefined arguments and snake model
            System.out.println("\n[ Test 4 with alternate format: ]");
            controller = new SnakeSerenadeController("test4_alt.csv", "0");
            controller.run();
            controller = new SnakeSerenadeController("test4_alt.csv", "1");
            controller.run();
            
	}//main

}//SnakeSerenade
