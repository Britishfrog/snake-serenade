package snakeserenade.model;

/**
 * The SingerSnake class is a singing snake that can link to others with its head and tail.
 * 
 * Methods provided are:
 *  -getHeadEndSnake
 *  -setHeadEndSnake(headEndSnake)
 *  -getTailEdnSnake
 *  -setTailEndSnake(tailEndSnake)
 *
 */

public class SingerSnake extends Snake {

    private Snake headEndSnake;
    private Snake tailEndSnake;
    
    public SingerSnake() {
    }

    public Snake getHeadEndSnake() {
        return headEndSnake;
    }

    public void setHeadEndSnake(Snake headEndSnake) {
        this.headEndSnake = headEndSnake;
    }

    public Snake getTailEndSnake() {
        return tailEndSnake;
    }

    public void setTailEndSnake(Snake tailEndSnake) {
        this.tailEndSnake = tailEndSnake;
    }

}