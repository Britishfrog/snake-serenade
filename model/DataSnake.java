package snakeserenade.model;

/**
 * The DataSnake class is a representation of the ensemble file snakes.
 * 
 * Methods provided are:
 *  -getHead
 *  -setHead(head)
 *  -getTail
 *  -setTail(tail)
 *
 */

public class DataSnake extends Snake {

    private int head;
    private int tail;

    public DataSnake() {
    }
    
    public int getHead() {
        return head;
    }

    public void setHead(int head) {
        this.head = head;
    }

    public int getTail() {
        return tail;
    }

    public void setTail(int tail) {
        this.tail = tail;
    }

}