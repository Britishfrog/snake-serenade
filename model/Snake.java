package snakeserenade.model;

/**
 * The Snake class is the basic snake.
 * 
 * Methods provided are:
 *  -setName(name)
 *
 */

public abstract class Snake {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}