package snakeserenade.model;

import java.util.Map;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * The SingerSnakeEnsemble class stores SingerSnakes in a LinkedHashMap.
 * 
 * Methods provided are:
 *  -addSnake(dataSnake)
 *  -addSnake(singerSnake)
 *  -getSingerSnake
 *  -getDataSnake
 *  -getSingerSnakeAtPos(position)
 *  -getSingerSnakesMap
 *  -getDataSnakesMap
 *
 */
public class SingerSnakeEnsemble {

    private Map<String, DataSnake> dataSnakesMap;
    private Map<String, SingerSnake> singerSnakesMap;

    public SingerSnakeEnsemble() {
    	this.dataSnakesMap = Maps.newHashMap();
        this.singerSnakesMap = Maps.newLinkedHashMap();
    }

    //Add data snake to hash map
    public void addSnake(DataSnake dataSnake) {	
    	dataSnakesMap.put(dataSnake.getName(), dataSnake);
    }
    
    //Add singer snake to linked hash map
    public void addSnake(SingerSnake singerSnake) {
        singerSnakesMap.put(singerSnake.getName(), singerSnake);
    }

    //Retrieve singer snake by name
    public SingerSnake getSingerSnake(String name) {
        return singerSnakesMap.get(name);
    }
    
    //Retrieve singer snake by position
    public SingerSnake getSingerSnakeAtPos(int pos) {
    	return Lists.newArrayList(singerSnakesMap.values()).get(pos);
    }

    //Retrieve singer snake map
    public Map<String, SingerSnake> getSingerSnakesMap() {
        return singerSnakesMap;
    }
    
    //Retrieve data snake map
    public Map<String, DataSnake> getDataSnakesMap() {
    	return dataSnakesMap;
    }

}//SingerSnakeEnsemble