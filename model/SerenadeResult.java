package snakeserenade.model;

/**
 * 
 * The Serenade Results class allows you to display the results of the snake serenade
 * 
 */

public class SerenadeResult {

	private int singCount;
	private String serenadeResult;
	
	public int getSingCount() {
		return singCount;
	}
	
	public void setSingCount(int singCount) {
		this.singCount = singCount;
	}
	
	public String getSerenadeResult() {
		return serenadeResult;
	}
	
	public void setSerenadeResult(String serenadeResult) {
		this.serenadeResult = serenadeResult;
	}
}