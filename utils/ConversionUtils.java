package snakeserenade.utils;

import org.apache.commons.lang3.StringUtils;

import snakeserenade.exception.ValidationException;
import snakeserenade.model.DataSnake;

/**
 * ConversionUtils is used to convert the data from the CSV file into DataSnakes
 */
public class ConversionUtils {

    private static final String CSV_FILE_DELIMITER = ",";
    
    public static DataSnake convertCsvLineToSnake(String line) {
        String[] data = line.trim().split(CSV_FILE_DELIMITER);
        if (data.length != 3) {
            throw new ValidationException("Invalid data format. Must be: <snake_name>, <tail_end>, <head_end>");
        }

        DataSnake snake = new DataSnake();
        String name = data[0].trim();
        if (StringUtils.isNotBlank(name)) {
            snake.setName(name);
        }
        String head = data[1].trim();
        if (StringUtils.isNotBlank(head)) {
            try {
                Integer headInt = Integer.valueOf(head);
                if (headInt >= 0) {
                	snake.setHead(headInt);
                } else {
                	throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                throw new ValidationException("Data is invalid: head=" + head);
            }
        }
        String tail = data[2].trim();
        if (StringUtils.isNotBlank(tail)) {
            try {
                Integer tailInt = Integer.valueOf(tail);
                if (tailInt >= 0) {
                	snake.setTail(tailInt);
                } else {
                	throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                throw new ValidationException("Data is invalid: tail=" + tail);
            }
        }
        return snake;
    }
    
    /**
     * The convertMaximumRepeats converts the maximumRepeats string into an integer
     */
    
    public static int convertMaximumRepeats(String maximumRepeats) {
    	int maximumRepeatsPerSnake = 0;
    	try {
    		Integer maximumRepeatsInt = Integer.valueOf(maximumRepeats);
			if (maximumRepeatsInt >= 0) {
				maximumRepeatsPerSnake = maximumRepeatsInt;
			} else {
				throw new NumberFormatException();
			}
    	} catch (NumberFormatException e) {
    		throw new ValidationException("Data is invalid: maximumRepeatsPerSnake=" + maximumRepeats);
    	}
    	return maximumRepeatsPerSnake;
    }

}